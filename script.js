/**
* Arquivo de funcoes javaScript da Calculadora Lulante
* autor: Maiser Jos� Alves Oliva
* License: GNU/GPL
*
*Observa��o: variaveis declaradas  localmente e repetidamente em cada funcao pois est� havendo erro em outros navegadores.
*/

/**
* Insere um luligito na calculadora
*/
function insere(luligito){
	
	// declarando variaveis
	var cnt1 = document.getElementById('cnt1');
	var cnt2 = document.getElementById('cnt2');
	var opr = document.getElementById('opr');
	var memoria = document.getElementById("result");
	var links = document.getElementsByTagName('span');
	//conta quantas tags span existem
	var count = links.length;
	var display = document.getElementById("d0");
	//maximo de digitos a serem iseridos na calculadora
	var max_digitos = 5;	
	
	/**
	*limpa o display ao ser inserido um novo numero ap�s o operador
	*/
	if((count>=2 && cnt2.value=="")){
		display.innerHTML = '';
		count=1;
	}
	
		if(count==1){
			
			var operador = document.calc.opr.value;
	
			display.innerHTML = '<span id="d'+count+'"><img src="img/'+luligito+'.jpg" align="middle"></span>';
			
			if(operador=="menos" && cnt1.value==""){
				//operador = operador.replace(/mais/,'+');
				operador = operador.replace(/menos/,'-');
				cnt2.value = operador+luligito;
				//alert('SINAL');
			}else{
				cnt2.value = cnt2.value+luligito;
			}
			

		}else if(count <= max_digitos){

			var nligito = '<span id="d'+count+'"><img src="img/'+luligito+'.jpg" align="middle"></span>';
	
			display.innerHTML = display.innerHTML += nligito; 
			cnt2.value = cnt2.value += cnt2.value=luligito;

		}else{
			alert('N�o � poss�vel adicionar mais lul�gitos.');
			//alert('Aten��o: M�ximo de lul�gitos permitido. Voc� pode solucinar este problema comprando uma calculadora HP Lulante por apenas BITROTROTROTROLA reais. Visite o site www.lulandia.trola.lelero');
		}
	
} 

/**
* Limpa o display e as variaveis da calculadora
*/	
function limpar(){

	var cnt1 = document.getElementById('cnt1');
	var cnt2 = document.getElementById('cnt2');
	var opr = document.getElementById('opr');
	var memoria = document.getElementById("result");
	var display = document.getElementById("display");
	
	document.getElementById("operador").innerHTML = '';
	document.getElementById("txtlandia").innerHTML = '';
	
	display.innerHTML = '<span id="d0"><img src="img/lero.jpg" align="middle"></span>';
	cnt1.value="";
	cnt2.value="";
	opr.value="";
	memoria.value = ""; 
}

/**
*funcao que realiza a operacao especificada pelo operador
*/
function operacao(operador){

	var cnt1 = document.getElementById('cnt1');
	var cnt2 = document.getElementById('cnt2');
	var opr = document.getElementById('opr');
	var display = document.getElementById("d0");
	var memoria = document.getElementById("result");
	
	document.getElementById("operador").innerHTML = '<img src="img/'+operador+'.jpg" />';
	
	//procura pelo operador matematico
	switch (operador) {
		case "sum": opr.value='mais'; break;
		case "sub": opr.value='menos'; break;
		case "mul": opr.value='vezes'; break;
		case "div": opr.value='dividido'; break;
	}
	
	//verifica memoria vazia e realiza a operacao
	if((cnt1.value!="" && cnt2.value!= "") && (memoria =="")){
		calcular();
		exit;
	}
	
	//verifica operacao de apenas uma conta
	if(cnt1.value!="" && cnt2.value== ""){
		exit;
	}
	
	cnt1.value = cnt2.value;
	cnt2.value = "";
}

/**
* Limpa somente o display da calculadora
*/	
function limpa_display(){
	var display = document.getElementById("display");
	display.innerHTML = '<span id="d0"><img src="img/lero.jpg" align="middle"></span>';
	document.getElementById("txtlandia").innerHTML = '';
}
/**
*Insere a virgula no local especificado
*/	
function insere_virg(){

	var cnt1 = document.getElementById('cnt1');
	var cnt2 = document.getElementById('cnt2');
	var opr = document.getElementById('opr');
	var display = document.getElementById("d0");
	var memoria = document.getElementById("result");
	var virgula = document.getElementById("virgula");

	//alert(virgula);
	if(virgula==null){
		cnt2.value = cnt2.value+= '.';
		display.innerHTML = display.innerHTML += '<img src="img/virg.jpg" align="middle" id="virgula">'; 
	}

}
/**
* Troca o sinal (Positivo) ou (Negativo) do n�mero corrente
*/
function mudar_sinal() {
	var cnt1 = document.getElementById('cnt1');
	var cnt2 = document.getElementById('cnt2');
	
	if(cnt2.value.search(/-/) ==-1){
	//alert('teste1');
		cnt2.value =  '-'+cnt2.value;
	}else{
	//alert('teste2');
		cnt2.value = cnt2.value.replace(/-/,'');
	}
	
}
/**
*Efetua a operacao ou o calculo das contas
*/
function calcular() {

    var conta1 = document.getElementById("cnt1").value;
	var conta2 = document.getElementById("cnt2").value ;
	var operador =	document.getElementById("opr").value;
	var memoria = document.getElementById("result").value;
	var display = document.getElementById("d0");
	
	document.getElementById("operador").innerHTML = '';
	
	//criando Java Script Ass�ncrono
	var xmlHttp = criaXMLHttp();
	
		if(memoria =='ERRO'){
			//alert("TST");
			limpar();
			exit;
		}
		
		
		//nao � possivel realizar a conta sem o operador	
		if (operador == ""){
			if(cnt2.value.search(/-/) != -1 ){
			//alert('teste');
			document.getElementById("opr").value = 'menos';
			operador = document.getElementById("opr").value;
			//display.innerHTML = '<span id=0><img src="img/sub.jpg" align="middle"></span>';
			}
			exit;
		}
		
		//transferencia de valores caso seja realizada conta com apenas 1 numero lulimal
		if(conta1 ==""){
			conta1 = conta2;
			conta2 ="";
		}

   
	xmlHttp.open("GET","require.php?conta1="+conta1+"&conta2="+conta2+"&operador="+operador+"&memoria="+memoria,true);
	//xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  
	xmlHttp.onreadystatechange=function() {
	
		if(xmlHttp.readyState==4){
			if(xmlHttp.status == 200) {

				var serv_return = xmlHttp.responseText.split("#");
				
				document.getElementById("d0").innerHTML = serv_return[0];
				document.calc.result.value = serv_return[1];
			
				if(serv_return[1]!='ERRO'){
					document.getElementById("txtlandia").innerHTML = serv_return[1];
				}else{
					document.getElementById("txtlandia").innerHTML = '';	
				}
				
			}else {
                document.getElementById("display").innerHTML = "Um erro ocorreu" + XMLHttp.statusText;
            }
		}
	}
 
	xmlHttp.send(null);
	//xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
}

/**
* Cria AJAX de acordo com a versao do navegador do usuario
*/
function criaXMLHttp() {

       if (typeof XMLHttpRequest != "undefined")
          return new XMLHttpRequest();
       else if (window.ActiveXObject){
          var versoes = ["MSXML2.XMLHttp.5.0",
          "MSXML2.XMLHttp.4.0", "MSXML2.XMLHttp.3.0",
          "MSXML2.XMLHttp", "Microsoft.XMLHttp"
          ];
       }
       for (var i = 0; i < versoes.length; i++){
          try{
              return new ActiveXObject(versoes[i]);
          }catch (e) {}
       }
       throw new Error("Seu navegador n�o suporta esta fun��o!");
}


  

