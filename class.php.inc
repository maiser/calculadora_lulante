<?php
/**
 * Class Calculadora Lulante
 * Metodos e funcoes para trabalhar aritmetica lulante
 * 
 * @version 1.0
 * @author Maiser Jos� Alves Oliva
 * @license GNU/GPL
 * 
 * //informacoes para manipulacao de bases
 * http://www.mathpath.org/concepts/Num/frac.htm
 * http://www.lia.ufc.br/~paty/icc/notas/4/index.html
 */

include('defines.php');

class AritmeticaLulante{
	
	/**
	 * Define a base da aritm�tica lulante
	 * @var int $base
	 */
	protected $base=4;
	/**
	 * Define a base que a aritmetica lulante sera convertida 
	 * (decimal ser� usada para operacoes)
	 * 
	 * @var int $to_base
	 */
	protected $to_base=10;
	
	/**
	 * Converte o n�mero Lulimal para o sistema decimal
	 *
	 * @param int $numero � o n�mero a ser convertido
	 * @return int $numDecimal � o numero convertido para decimal
	 */
	function convertLulanteToDecimal($numero){
		
		$i = 0;
		//echo $num_conv = $numero.'-';
		$fracao = 0;
		
	//verifica se o numero n�o � inteiro
	if(!(is_int($numero))){
		
		$frac = explode('.',$numero);
		
		$pos = strlen($frac[1]);
		for($i=0; $i<$pos;$i++){
		
	       		$digito = substr($frac[1],$i,1);
	       		$fracao += ($digito) * (pow($this->base,-($i+1)));
		}
			//$fracao = str_replace(array(4,5,6,7,8,9),array(0,1,2,3,0,1),$fracao);
	}
	
		/**
		 * base_convert(numero, base do numero, base a ser convertida);
		 */
		$numDecimal = base_convert(intval($numero), $this->base, $this->to_base);
		//echo ' dec: '.$numDecimal.' ';
		//echo $numDecimal;
		//restaurando o sinal apos a conversao
		if($numero < 0){
			return(-$numDecimal+$fracao);
		}else {
			return($numDecimal+$fracao);
		}
	
	}
	
	/**
	 * Converte um n�mero Decimal para o sistema Lulimal
	 *
	 * @param int $numero � o n�mero a ser convertido
	 * @return int $numLulimal � o numero convertido para decimal
	 */
	function convertDecimalToLulante($numero){
	
		$i = 0;
		$num_conv = $numero;
		$fracao = '';
		
	//verifica se o numero n�o � inteiro
	if(!(is_int($numero))){
		
		$index=-1;
		while ($num_conv > 0){
			
			if($i==4){
	        	break;
	        }
	         
	       $num_conv = $num_conv * $this->base ; // multiplicando o numero pela base
	
	        // se o numero for maior que 1 
	        if ($num_conv >= 1) {
	        	//concatena a parte inteira na variavei digito
	            $digito[$index] = intval($num_conv);
	             
	             // subtrai a parte inteira do proprio numero para obter uma fracao
	             $num_conv -=  intval($num_conv); 
	         }else {// se $num < 1 entao a parte inteira � zero
	         	
	            $digito[$index] = 0 ;
	         }
	        $index--;
	     }

	     for($i=-1;$i>$index;$i--){
	     	if(($digito[$i] % 4) == 0){
	     		$digito[$i] = 0;
	     	}if(($digito[$i] % 4) == 1){
	     		$digito[$i] = 1;
	     	}if(($digito[$i] % 4) == 2){
	     		$digito[$i] = 2;
	     	}if(($digito[$i] % 4) == 3){
	     		$digito[$i] = 3;
	     	}
	     	$fracao .= $digito[$i];
	     }
	    // var_dump($fracao);
	     $fracao = '.'.$fracao;
	}
	
		//base_convert(numero, base do numero, base a ser convertida);
		//converte apenas a parte inteira do numero decimal
		$numLulimal = base_convert(intval($numero), $this->to_base, $this->base);
		
		if($numero < 0){
			return(-$numLulimal.$fracao);
		}else {
			return($numLulimal.$fracao);
		}

	}
		
	/**
 	* Converte em s�mbolos lulantes o n�mero representado na artimetica lulante
 	*
 	* @param int $numero
 	* @return string $image
 	*/
	function convertToImage($numero){
		
		$numero = str_replace('.',VIRGULA,$numero);
		
		for($i=0; $i <= 3; $i++){
		
			$replace = str_replace($i,constant(LULANTE.$i), $numero);
			$numero = $replace;
		}
		
		return($numero);
	}
	
	/**
	 * Converte o texto lulimal para numeros representativos (base lulimal)
	 *
	 * @param string $texto
	 * @return int lulimal $texto
	 */
	function convertTextToNumber($texto){
		//convertendo a string para caixa-alta (uppercase)
		$texto = strtoupper($texto);
		
		//substituindo texto por numero
		$numero = str_replace('LERO',LERO,$texto);
		$numero = str_replace('LULA',LULA,$numero);
		$numero = str_replace('BILA',BILA,$numero);
		$numero = str_replace('TROLA',TROLA,$numero);
		$numero = str_replace('LE',LERO,$numero);
		$numero = str_replace('LU',LULA,$numero);
		$numero = str_replace('BI',BILA,$numero);
		$numero = str_replace('TRO',TROLA,$numero);
	
		//verfica se a variavel $texto � n�m�rica
		if(is_numeric($numero)== true){
			
			return($numero);
			
		}else {
		
			return('Erro: digite um n&uacute;mero valido!');
		}
	}
	
/**
	 * Converte o numero lulimal para texto formato (trola, lubilero..etc)
	 *
	 * @param string $numero
	 * @param string $char_after (caso precise adicionar caracteres no inicio da string)
	 * @return int lulimal $numero (caso precise adicionar caracteres no final da string)
	 */
	function convertNumberToText($numero,$char_before="", $char_after=""){

		//pegando o ultimo digito do numero lulimal
		$last_digit = substr($numero,-1);
		
		//separando o ultimo digito da string de todo resto
		$texto = str_replace(LERO,'LE',substr($numero,0,-1));
		$texto = str_replace(LULA,'LU',$texto);
		$texto = str_replace(BILA,'BI',$texto);
		$texto = str_replace(TROLA,'TRO',$texto);
		
		//o ultimo digito � traduzido diferente dos antateriores
		//traducao: LERO, LULA, BILA, TROLA
		$texto = str_replace(TROLA,'TROLA',$texto.$last_digit);
		$texto = str_replace(LERO,'LERO',$texto);
		$texto = str_replace(LULA,'LULA',$texto);
		$texto = str_replace(BILA,'BILA',$texto);

	

		//verfica se a variavel $numero � n�m�rica
		if(is_string($texto)== true){
			
			return($char_before.$texto.$char_after);
			
		}else {
		
			return('Erro: digite um n&uacute;mero valido!');
		}
	}
	
	/**
	 * Soma n�meros lulimais e retorna o total
	 *
	 * @param string $parcelas
	 * @param string $separador
	 * @param float $memoria
	 * @return $total
	 */
	function SomaLulante($parcelas, $separador, $memoria){
		
		//parcelas em array
		$parcela_div = explode($separador,$parcelas);
		
		$parcela1 = $this->convertTextToNumber($parcela_div[0]);
		$parcela2 = $this->convertTextToNumber($parcela_div[1]);
		
		//echo $parcela1.'+'.$parcela2.' | ';
		
		$parcela1 = $this->convertLulanteToDecimal($parcela1);
		$parcela2 = $this->convertLulanteToDecimal($parcela2);
		
		
		//somando as parcelas
		$soma = $parcela1 + $parcela2;
		
		//se existir alguma conta na memoria
		if(!(empty($memoria))){
			$memoria = $this->convertTextToNumber($memoria);
			$memoria = $this->convertLulanteToDecimal($memoria);
			$soma =  $memoria + $parcela2;
		}
		
		$total = $this->convertDecimalToLulante($soma);
		
		return($total);
	}
	
/**
	 * Subtrai n�meros lulimais e retorna o resultado
	 *
	 * @param string $parcelas
	 * @param string $separador
	 * @param float $memoria
	 * @return $total
	 */
	function SubtracaoLulante($parcelas, $separador,$memoria){
		
		//parcelas em array
		$parcela_div = explode($separador,$parcelas);
	
		$minuendo = $this->convertLulanteToDecimal($this->convertTextToNumber($parcela_div[0]));
		$subtraendo = $this->convertLulanteToDecimal($this->convertTextToNumber($parcela_div[1]));

		//subtraindo as parcelas
		$subtracao = $minuendo + (-$subtraendo);
		
		$total = $this->convertDecimalToLulante($subtracao);
		
		//se existir alguma conta na memoria
		if(!(empty($memoria))){
			
			$memoria = $this->convertTextToNumber($memoria);
			$memoria = $this->convertLulanteToDecimal($memoria);
				
			if($subtraendo < 0){
				$total = $this->convertDecimalToLulante($memoria +($subtraendo));
			}else{
				$total = $this->convertDecimalToLulante($memoria + (-$subtraendo));	
			}
		}
		if($total==0){
			$total = str_replace('.','',$total);
		}
		return($total);
	}
	
/**
	 * Multiplica n�meros lulimais e retorna o produto
	 *
	 * @param string $fatores
	 * @param string $separador
	 * @param float $memoria
	 * @return $produto
	 */
	function MultiplicacaoLulante($fatores, $separador,$memoria){
		
		//parcelas em array
		$parcela_div = explode($separador,$fatores);
		
		$multiplicando = $this->convertLulanteToDecimal($this->convertTextToNumber($parcela_div[0]));
		$multiplicador = $this->convertLulanteToDecimal($this->convertTextToNumber($parcela_div[1]));

		//somando as parcelas
		$produto = $multiplicador * $multiplicando;
		
		$produto = $this->convertDecimalToLulante($produto);
		
		//se existir alguma conta na memoria
		if(!(empty($memoria))){
			
			$memoria = $this->convertTextToNumber($memoria);
			$memoria = $this->convertLulanteToDecimal($memoria);
	
			$produto = $this->convertDecimalToLulante(($memoria) * ($multiplicador));
		
		}
		
		return($produto);

	}
	
/**
	 * Divide os n�meros lulimais e retorna o quociente
	 *
	 * @param string $conta
	 * @param string $separador
	 * @param float $memoria
	 * @return float $produto
	 */
	function DivisaoLulante($conta,$separador, $memoria){
		
		$array_div = explode($separador, $conta);
		
		$dividendo= $this->convertLulanteToDecimal($this->convertTextToNumber($array_div[0]));
		$divisor = $this->convertLulanteToDecimal($this->convertTextToNumber($array_div[1]));
		
		//correcao para divisao por zero
		if($divisor == 0){
			return('ERRO');
			exit;
		}
		
		$quociente = ($dividendo / $divisor);
		$divisao = $this->convertDecimalToLulante($quociente);
		
		if(!(empty($memoria))){
			
			$memoria = $this->convertTextToNumber($memoria);
			$memoria = $this->convertLulanteToDecimal($memoria);
	
			$divisao = $this->convertDecimalToLulante(($memoria) / ($divisor));
		}
			
		return(round($divisao,4));		
	}

}//END CLASS

?>